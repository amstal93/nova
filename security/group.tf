resource "aws_security_group" "web-client" {
	name		= "web-client"
	description = "Egress access to public Internet on HTTP & HTTPS"
	egress {
		from_port 	= "80"
		to_port		= "80"
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	egress {
		from_port 	= "443"
		to_port		= "443"
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
}

resource "aws_security_group" "sql-server" {
	name        = "sql-server"
	description = "Ingress SQL"	
}

resource "aws_security_group" "web-server" {
	name        = "web-server"
	description = "Ingress HTTP ports 80 & 443"
	ingress {
		from_port		= "80"
		to_port			= "80"
		protocol		= "tcp"
		cidr_blocks 	= ["0.0.0.0/0"]
	}
	ingress {
		from_port		= "443"
		to_port			= "443"
		protocol		= "tcp"
		cidr_blocks		= ["0.0.0.0/0"]
	}
}

resource "aws_security_group" "ssh-server" {
	name        = "ssh-server"
	description = "Ingress SSH port 22"
}

output "sgs" {
	value = {
		web-client 	= aws_security_group.web-client.id,
		web-server	= aws_security_group.web-server.id,
		ssh-server	= aws_security_group.ssh-server.id,
	}
}