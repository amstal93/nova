.PHONY: help

help: ## Show the recipes in this Makefile. Like so `make help`
	@echo "----------------------------------------"
	@echo "|            Makefile help             |"
	@echo "----------------------------------------"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

CMD=terraform
TRS=$($@)

.AUTO: 
	# Comment out to stop automatic acceptence
	@echo "AUTO_ACCEPT is enabled in the Makefile by default for this project."
	$(eval export AUTO_ACCEPT=-auto-approve)

.FORCE:
	
silent:
	@:

DEBUG: ## Enable TF_LOG to some more information
	$(eval export TF_LOG=DEBUG)

.TARGET: ## REGIONs that the required TARGET is set
	$(if $(value TARGET),, $(if $(value ADDRESS), $(eval export TARGET=$(ADDRESS)), @printf "\033[31m%s\033[0m " Error; echo Please set the value of TARGET; make help; exit 1))

firstinit: ## Automatically run init if it has not yet been done.
	@[ -d .terraform ] || $(CMD) init

init: .FORCE ## Init to initialize Terraform for this project. Also used when Terraform is upgraded.
	$(CMD) init

creadentials: ## Populates the credentials file from the AWS files.
	@jq -Rn '{ "region": "'$(shell aws configure get region)'", "access_key": "'$(shell aws configure get default.aws_access_key_id)'", "secret_key": "'$(shell aws configure get default.aws_secret_access_key)'" }' > credentials.json

validate: .FORCE firstinit creadentials ## Check the syntax of the plans
	$(CMD) validate
	@find . -type f -name "*.sh" -exec bash -n "{}" \;

plan: .FORCE validate ## Generate the deployment plan. Must be proceeded by profile 'make US plan'
	$(CMD) plan $(if $(value TARGET), "-target=$(TARGET)", ) -var-file=./credentials.json

.SSHKEY: # Generate SSH key for the instance
	[ -f ./AWS_ALX_ssh_key ] || ssh-keygen -t rsa -b 2048 -f ./AWS_ALX_ssh_key -q -N ""
	$(if $(value SSH_AGENT_PID), $(shell ssh-add ./AWS_ALX_ssh_key), )

apply: .SSHKEY .FORCE validate #.AUTO validate ## Deploy the plans
	time $(CMD) apply $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) -var-file=./credentials.json 2>&1 | tee apply.log
	$(CMD) output
	
list: .FORCE ## List all resources currently in 'state'
	$(CMD) state list

show: .TARGET .FORCE ## Show the 'state' of a given resource. Requires TARGET=<MODULE> (AKA Address)
	$(CMD) state show $(TARGET)

taint: .TARGET .FORCE ## Force replace on next apply. Requires TARGET=<MODULE> (AKA Address)
	$(CMD) taint $(TARGET)

destroy: .FORCE #.AUTO ## To remove everything that Terraform built with apply.
	$(CMD) destroy $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) -var-file=./credentials.json 2>&1 | tee destroy.log

# https://github.com/hashicorp/terraform-provider-aws/issues/4982
destroyall: .FORCE .AUTO ## Destroy instance before security groups, workaround unresolved Terraform bug.
	$(CMD) destroy -target=module.instance.aws_instance.alx $(AUTO_ACCEPT) -var-file=./credentials.json 2>&1 | tee destroy.log
	$(CMD) destroy $(AUTO_ACCEPT) -var-file=./credentials.json 2>&1 | tee -a destroy.log
