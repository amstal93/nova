# variable "app_db" {
# 	type = map
# }

variable "instance-username" {
	type = string
}

variable "music-dir" {
	type = string
}

variable "sgs" {
	type = map
}

variable "ssh_keys" {
	default = {
		private_key = "./AWS_ALX_ssh_key"
		public_key	= "./AWS_ALX_ssh_key.pub"
	}
	description = "Locally generated key pair."
}
