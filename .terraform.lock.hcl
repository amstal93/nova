# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cyrilgdn/postgresql" {
  version     = "1.9.0-pre1"
  constraints = "1.9.0-pre1"
  hashes = [
    "h1:HoUKlxkjfbAv1wmdIYgQzpPTQdDFQYwW+Y3uyTBOj2Y=",
    "zh:31d8936752efc03e61980e5e22d8201d17aff19e8d29a6bc77e7d7080198bfe6",
    "zh:4bfb91948aa1db9282f247b774832b9158cd26a089634b70de2e1d4c95e03622",
    "zh:64319f5dc9f1f39581c8737c60a69ccb68f6ed4d230f608f5ac5a11c1045dfc6",
    "zh:911b0c527812b646a93467508f90b0afa0d826303966eb842ba303b23cf38fd7",
    "zh:915a49b757b99de4f1ff11fdb72e73b9117f48497101885ab7b431dff2d6c00b",
    "zh:c387198ee9f69763e87f5de199be0a8a65a42fb93252af02a95d4b1773de8b94",
    "zh:c3966b622d5bda43223da71cd02f98a111037e6e9441f0ec0e8efcf925393b6b",
    "zh:d390039b64642f0d6de36e9c3600192518a75b147c7436accb05d2a3d45a7acf",
    "zh:e035d18a288f6b6f79a2b6ad9641ea88849d9db5143958cc10000a83ac3e64af",
    "zh:e16a2edf2cc72610aa833fc55b12a6fdb656a190c8a8a72c716fad72801e0b02",
    "zh:ec4b1ea4ebbace5cb3a90e0585ab10aae6ee839441d73c58e7f0396e03bb73c1",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.29.1"
  constraints = "~> 3.27"
  hashes = [
    "h1:vFaEB2VBkZnKUXuVbOLbq6a8mOYNx2ftmTRP0YVbzu8=",
    "zh:2376f60f59d9c49277ea18d02f6ae316255df214046be87eacdad6ed8e56ba10",
    "zh:48dfd5ad70f26d794911413db14d957abb0bdff5000d5cb486464156201fcdf2",
    "zh:8aec3e84117df4195372ef58a2fd52900338156725da24d62fe7daab14d3ecae",
    "zh:b51fcbc7f2a6d78dfccb82b827f7f9da1ac1e51a5b7b3784f36de3e53b0e963d",
    "zh:c3df4b044020e4a5e13d6532d4e0ba191719b87131ccee07bd35dc819a09412d",
    "zh:c5195102a6411db6a5b96492f5dbe5a637e8063365efc14d13b05cb5e2764e8c",
    "zh:d57da72f95d0d8498d1c42a62241c93c5e30538471585177df6ea9280bf55de3",
    "zh:dec04bb83e3b6424f4e18ce0c48c14f3c75fbdd0d4f9181cb56f846fa9ac51f9",
    "zh:e9b4c360d6572db70e29be95076f2543a562420262c906cff710e662d6223247",
    "zh:ecb606aee05da1b2033874b5a9916d6d2877e748d5478a0d70b6fc4b5d08ab11",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version = "2.1.0"
  hashes = [
    "h1:HmUcHqc59VeHReHD2SEhnLVQPUKHKTipJ8Jxq67GiDU=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.1.0"
  hashes = [
    "h1:fUJX8Zxx38e2kBln+zWr1Tl41X+OuiE++REjrEyiOM4=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}
